<?php

namespace Bcfigueira\Frontend\Http;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

class HttpServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Bcfigueira\Frontend\Http\Controllers';
    /**
     * Lista de controllers a serem carregados
     */
    protected $listControllers = [
        // adicionar nome dos novos controllers aqui
        'ExempleController',
        'VersionControlController',
        // ...
    ];
    /**
     * path routes
     */
    protected $listRoutes = [
        // adicionar nome dos novos arquivos de rotas aqui
        'api' => __DIR__ . '/routes/api.php',
        'web' => __DIR__ . '/routes/web.php',
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadControllers();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutes();
    }

    /**
     * Load Controllers
     */
    protected function loadControllers()
    {
        foreach ($this->listControllers as $controller) {

            $this->app->make($this->namespace . '\\' . $controller);
        }
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    protected function loadRoutes()
    {
        // configura o arquivo de rotas dentro do pacote de Frontend\Http
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group($this->listRoutes['api']);

        Route::middleware('web')
            ->namespace($this->namespace)
            ->group($this->listRoutes['web']);
    }
}
