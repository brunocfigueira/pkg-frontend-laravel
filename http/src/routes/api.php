<?php

use Illuminate\Http\Request;

Route::middleware('api')->get('/teste', function (Request $request) {
    return $request->user();
});

Route::get('displayAllApi', 'VersionControlController@displayAllApi')->name('displayAllApi');    
Route::group(['as' => 'versioncontrol.', 'prefix' => 'versioncontrol'], function () {
    
});