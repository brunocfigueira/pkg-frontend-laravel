<?php

/**
 * how use route example 
 * reference site laravel
 * https://laravel.com/docs/6.x/routing
 */
Route::resource('exemples', 'ExempleController');
/**
 * routes to version control
 */
Route::group(['as' => 'versioncontrol.', 'prefix' => 'versioncontrol'], function () {
    Route::get('displayAll', 'VersionControlController@displayAll')->name('displayAll');
});
