<?php

namespace Bcfigueira\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Bcfigueira\Backend\Services\Rules\ExempleService;
use Bcfigueira\Backend\Exceptions\BusinessException;
use Bcfigueira\Frontend\Http\Traits\MessagesTraits as Messages;

class ExempleController extends Controller
{
    use Messages;
    private $exempleService;
    public function __construct(ExempleService $exempleService)
    {
        $this->exempleService = $exempleService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        //return response()->json(['exemples' => $this->exempleService->findAll()], 200);
        return responder()->success(['exemples' => $this->exempleService->findAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exemple  $exemple
     * @return \Illuminate\Http\Response
     */
    public function show(Exemple $exemple)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exemple  $exemple
     * @return \Illuminate\Http\Response
     */
    public function edit(Exemple $exemple)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exemple  $exemple
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exemple $exemple)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exemple  $exemple
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exemple $exemple)
    {
        try {
            $this->service->delete($exemple->id);
            $message = $this->service->msgSuccessDeleted(trans('menu.model'));
            return response()->json(['success' => true, 'message' => $message], 200);
        } catch (BusinessException $ex) {
            return response()->json(['success' => false, 'message' => $ex->getMessage()], 200);
        }
    }
}
